# nemosub

A utility to launch multiple Nemo's simulations on a cluster from a single configuration file. Works for all main types of cluster scheduler (Slurm, OAR, Moab/Torq etc.)

To compile nemosub, place the `nemosub.cc` file in the `nemo` base folder, that is above the `src/` folder containing all source files of nemo. Also copy the `Makefile-nemosub` there. Then call `make`:

First, clean up from previous build:

    make clean

then compile and install:

    make -f Makefile-nemosub install

The `install` option above will tell make to copy the nemosub executable from the `bin` directory to the install location set in the Makefile file.

For instruction about how to use `nemosub`, read the [MANUAL](https://bitbucket.org/ecoevo/nemosub/wiki/Home) (nemosub Wiki page).

\FG

