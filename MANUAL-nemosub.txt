NEMOSUB -- the nemo job scheduler --

TOC:
-SYNOPSIS
-DESCRIPTION
-HOW TO
-NEMOSUB PARAMETERS
  -SHORT DESCRIPTION
  -DESCRIPTION
-NEMOSUB PARAMETERS EXPANSION

-------------------
SYNOPSIS

nemosub FILE [FILE...]


-------------------
DESCRIPTION

**nemosub** is a job submission tool designed to submit and run multiple nemo simulations on high performance computer clusters. **nemosub** works with different job schedulers (e.g., bsub, sbatch, oarsub, ...)

**nemosub** uses one or many nemo parameter file(s) (init file) provided as input on the command line. nemosub will then submit a separate job for each simulation found in the init file(s). The job parameterisation can be done in the init file(s) directly by adding nemosub-specific parameters to them, for instance to set the number of CPUs, the time limit, the execution queue or any other scheduler and cluster specific parameter. It can also be used to launch MPI jobs together with nemo_mpi.

**nemosub** proceeds by reading the init file(s), creates a simulation-specific init file that is then used to submit a job. Job submission is dependent on the type of scheduler (bsub, sbatch, ...) and nemosub is configured to use scheduler-specific commands. The submission is customizable via nemosub-specific parameters added to the init file(s) or in a submission script template. The script template is written in bash scripting language. An example is provided below and in the `example` folder of the repository. The script template holds the command to execute nemo with the name of the init file created by nemosub passed as argument to the script (see examples below). Nemosub generates a submission script from the script template and the scheduler parameterisation found in the init file(s).

By default, nemosub expects to find a set of folders in its working directory  (the `jobs/` and `jobs/init` directories) to store the parameter files of each simulation (in `jobs/init`) and to log the launched simulations in a file named `launched_jobs` (in `jobs/`). The job output files created by the job scheduler (standard and error outputs) are also saved in the `jobs` folder. This behavior can be adjusted with specific parameters within the nemo input file.

-------------------
HOW TO

TOC:
1/ create the job submission environment (mkdir -p jobs/init)
2/ create the submission script template (edit sub_template)
3/ create the nemo input parameter file with nemosub parameters (edit mySim.ini)
4/ submit the jobs with nemosub (nemosub mySim.ini)
5/ what happened?


Here is how it goes:

** 1/ create the job submission environment:

  $ mkdir -p jobs/init

optionally, you can create a directory to store the init files of failed jobs:

  $ mkdir failed_jobs

the copying of init files into that folder upon job failure must be handled in the submission script template (see example below).



** 2/ create the submission script template (here for a Slurm scheduler). The file is called `sub_template` and placed in the folder from which nemosub is invoked.

Edit the script template `sub_template`, here written for a SLURM cluster using `sbatch` to submit jobs:

$ vi sub_template

    #SBATCH --mem-per-cpu=8000
    #SBATCH --time=8:0:0

    ## these two options above can be specified in the init file directly as:
    ## sub_parameters (--mem-per-cpu=8000, --time=8.0.0)

    ## POSITIONAL VARIABLES:
    ## $1: by default, the positional variable $1 always holds the name of the init file of the simulation
    ## $2, $3, ...:  more arguments can be passed to the script from within the init file with parameter 'sub_script_args'


    ## Grab the name of the init file:
    ##  by default, the path to the init file starts with 'jobs/init/', which we want to remove:

    FNAME=`basename $1`   ## remove all leading '/' from input file name

    ## Grab a few info from the init file:

    ## the simulation output name:
    simname=`awk '/^filename/{if($2 != "")print $2}' $1`  ## here $2 is specific to awk, it is not an argument to the script

    ## the root output dir of the simulation:
    root_dir=`awk '/^root_dir/{print $2}' $1`

    command="$HOME/bin/nemo2.4.0 $1"   ##IMPORTANT this is where you choose which version of nemo to run
                                        # nemo could also be an argument to the script (eg. positional variable $2) if specified in the init file
                                        # e.g.: sub_script_args nemo2.4.0 (in the init file passed to nemosub)
                                        # in that case, the command becomes:
                                        #    command="$HOME/bin/$2 $1"
                                        # or if nemo is in the current working directory:
                                        #    command="./$2 $1"
                                        # note that it is not necessary to add the full path if nemo in your PATH

    echo "executing $command"          ## message will appear in the job std output file

    ## now execute the command:

    $command

    exitstatus=$?


    ## POST-PROCESSING ##

    # look for the execution status into the simulation log file
    check=`grep -c finished ${root_dir}/${simname}.log`

    # check execution status of the simulation
    if [ $exitstatus -ne 1 -a $check -eq 1 ];

       then
        echo "normal exit: " $1
    #   mv $1 completed_sims/
    # or:
    #   rm $1     # remove the init file from the jobs/init/ folder

    # Option:
    # archive result files; add '--remove-files' to remove original files
    #	tar cfz ${simname}.tgz $root_dir/$simname*

    else
        echo "abnormal exit: " $1
    #    mv $1 failed_sims/
    fi

    # Tar result files; this removes the original files !!
    #tar cfz  ${simname}.tgz $root_dir/$simname* --remove-files

    # example for cases when output files are in different folders:
    #tar cfz  ${simname}.tgz $root_dir{/,/ntrl/,/quanti/}$simname* --remove-files

  :wq  #save sub_template and quite vi



** 3/ set the nemo input parameter file with nemosub parameters

Edit the init file (`mySim.ini` in the `example folder`):

$ vi mySim.ini

    ### nemosub params ###
    sub_program                sbatch
    sub_script_directive       SBATCH
    sub_parameters             (--ntasks=1, --account=xyz, --partition=large)
    sub_script_args            nemo2.4.0

    sub_jobname                p%1q%2-r%3

    ### nemo params ###
    run_mode run
    root_dir                   sim-campaign-1

    filename                   mySim%1-%2-%3

    random_seed 32001%'1[+]'3%'2[+]'3  #IMPORTANT set a different seed for each replicate!!!!
               #will be: 3200101, 3200202, 3200303, ...
    replicates  1 1 1 1 1 1 1 1 1 1 1 #one job per replicate, this is sequential param %3

    generations 10000

    ## POPULATION ##
    [...]

  :wq  #save and quite



** 4/ submit the jobs with nemosub:

$ nemosub mySim.ini



** 5/ what happened?

for each simulation, nemosub has done the following:

- create a simulation parameter file in `jobs/init` for each combination of parameter values contained in the input file. Each simulation parameter file gets a special name based on the 15 first characters of the simulation 'filename' parameter value, to which a unique random code is added to make sure several invocations of nemosub will not overwrite init files of simulation jobs that are not yet executed (queued). The code starts with 4 random letters followed by a number, first randomly drawn between 0-800, and incremented for each simulation job submitted.

- create a submission script based on the scheduler parameters found in the nemo input file and on the content of the `sub_template` file. The submission script is called `sub000`, saved in the local working directory and overwritten by each job submission.

eg. of sub000 for oarsub:
   #!/bin/bash
   #OAR -n camp0001_g11-5
   #OAR -E jobs/camp0001_g11-5.e%jobid%
   #OAR -O jobs/camp0001_g11-5.o%jobid%
   #OAR -l nodes=1/core=8,walltime=96:00:00
   #OAR --project teamx

   filename=`basename $1` ##remove all leading '/' from input file name

   [...]

- execute a system command to submit each simulation job, calling the specified scheduler command and passing it the submission script as argument. The submission command is of the form:

    sub_program {< ,./}sub000 jobs/init/[filename][code].ini {any additional arguments from input file}

command for Slurm/sbatch or PBS/qsub:

    sbatch sub000 jobs/init/mySimHuTr789.ini

OAR:
    oarsub ./sub000 jobs/init/mySimHuTr789.ini

LSF/bsub:

    bsub < sub000 jobs/init/mySimHuTr789.ini

in all cases, sub000 is made executable before submission (chmod 755 sub000).

- logged the simulation name, job name, input file used, and the output of the scheduler (job id) in `jobs/launched_jobs`. This comes handy when trying to link a job id to a particular simulation.

eg:
$ head jobs/launched_jobs
campanula_g1-1 camp-g1-1 jobs/init/campanula_g1-1WGEZ510.ini Submitted batch job 515
campanula_g1-2 camp-g1-2 jobs/init/campanula_g1-2WGEZ511.ini Submitted batch job 516
campanula_g1-3 camp-g1-3 jobs/init/campanula_g1-3WGEZ512.ini Submitted batch job 517
campanula_g1-4 camp-g1-4 jobs/init/campanula_g1-4WGEZ513.ini Submitted batch job 518
campanula_g1-5 camp-g1-5 jobs/init/campanula_g1-5WGEZ514.ini Submitted batch job 519
campanula_g2-1 camp-g2-1 jobs/init/campanula_g2-1WGEZ515.ini Submitted batch job 520
campanula_g2-2 camp-g2-2 jobs/init/campanula_g2-2WGEZ516.ini Submitted batch job 521
campanula_g2-3 camp-g2-3 jobs/init/campanula_g2-3WGEZ517.ini Submitted batch job 522
campanula_g2-4 camp-g2-4 jobs/init/campanula_g2-4WGEZ518.ini Submitted batch job 523
campanula_g2-5 camp-g2-5 jobs/init/campanula_g2-5WGEZ519.ini Submitted batch job 524




-------------------
NEMOSUB PARAMETERS

The following parameters must be put in the nemo parameter file passed to nemosub on the command line. Only two are mandatory, the 'sub_program' giving the name of the scheduler command, and 'sub_script_directive' to specify the scheduler tag that must be used in the submission script (e.g., #SBATCH, #OAR, etc.) These parameters will be parsed by nemosub but not passed down to nemo when creating the simulation parameter files.

SHORT DESCRIPTION:
------------------

sub_program  [mandatory] {no default} the name of the scheduler command

sub_script_directive  [mandatory] {no default} the script scheduler tag (#TAG)

sub_script_header  [optional] {#!/bin/bash} the shebang of job submission scripts

sub_script_args  [optional] {no default} arguments passed to the job submission script

sub_append_ini_to_script  [optional, true if present] {false} append filename to script

sub_input_dir  [optional] {jobs/init/} location of nemo init parameter files

sub_output_dir  [optional] {jobs} where to save output scheduler files (stdout & stderr)

sub_jobname  [optional] {same as simulation 'filename'} the name of the job

sub_queue  [optional] {no default} queuing option added to the submission script

sub_parameters  [optional] {no default} additional options to the scheduler

sub_resources  [optional] {no default} additional resource options to the scheduler



DESCRIPTION:
------------

sub_program  [mandatory]

   This parameter specifies the name of the scheduler command used on your cluster to start jobs (e.g., sbatch, bsub, qsub, oarsub)



sub_script_directive  [mandatory]

    The script directive is the tag used in a Bash submission script to pass specific options to the scheduler. This is used instead of specifying the options on the command line. For example, a Slurm scheduler uses #SBATCH, an oar scheduler uses #OAR, etc.



sub_script_header  [optional]

    The "shebang" line of your submission script (i.e., the first line of a shell script). Defaults to '#!/bin/bash'.

    NOTE: for PBS (qsub), the Bourne shell is specified in the submission script as:

    #PBS -S /bin/bash



sub_script_args  [optional]

    Specific arguments that must be passed to the submission script. Multiple arguments can be passed and must be placed within parentheses, separated by commas: (arg1, arg2, arg3). It is up to the user to provide a script template that correctly deals with those arguments. IMPORTANT: by default, the first argument to the script is the name of the nemo input file of the current simulation created by nemosub, and will be referenced by the positional variable $1 within the script. The other arguments will have positional variables $2, $3, $4, etc. in the same order as within the parentheses above.



sub_append_ini_to_script  [optional, true if present]

    This boolean parameter modifies the above behavior of passing the name of the nemo input file of the current simulation as first argument to the submission script. Instead, nemosub writes the name of the file in the submission script directly, appending it to the script template (i.e. will come after the last character of the `sub_template` file).

    The name of the init file written in the submission script is not the name of the file passed as argument to nemosub. It is the name of the file created by nemosub for the current simulation. By default, that file is located in 'jobs/init' in the current working directory, or in the directory specified with 'sub_input_dir'.

    In order for the script to work, the sub_template file MUST end with a command of the sort:

    nemox.y.z [the filename will be written here, including directory path]

    that is, it must hold the command to be executed to run the simulation. It is up to the user to make sure that the nemo command can be executed (make sure it is on your PATH, or specified with the full path).

    By default, nemosub then adds one extra line to the submission script after appending the init file name:

    rm -f jobs/init/xxxxx.ini \n

    therefore, if the nemo command exits normally (code 0), then the simulation init file is erased from the jobs/init/ folder. It is a way to control for failed jobs.

    The scheduler command is then: `sub_program {< ,./}sub000 {+ any additional arguments from sub_script_args}`


sub_input_dir  [optional]

    Used to specify the directory where the nemo init parameter files will be stored. Defaults to 'jobs/init' in the current working directory (where simulations are started and jobs are scheduled).



sub_output_dir  [optional]

    Directory to store the scheduler output files (stdout and stderr). Defaults to 'jobs/' in the current working directory.



sub_jobname  [optional]

    Use this to craft job names, passed to the scheduler, according to your simulation parameters. By default, if not specified, the job name is the same as nemo's 'filename' parameter value.

    It is usually advised to have short job names, shorter than the usual simulation filename. Expansion characters for sequential parameter values (i.e., %1, %'.3'2, etc.) can be used and will be expanded.

    That job name will then be passed to the scheduler, and written in the submission script to specify the standard and the error output files:

    e.g. for Slurm:
    #SBATCH -J 'jobname'
    #SBATCH -o 'output_dir/jobname'.o%j
    #SBATCH -e 'output_dir/jobname'.e%j

    e.g. for OAR:
    #OAR -n 'jobname'
    #OAR -O 'output_dir/jobname'.o%jobid%
    #OAR -E 'output_dir/jobname'.e%jobid%

    e.g. for bsub:
    #BSUB -J 'jobname'
    #BSUB -oo 'output_dir/jobname'.o%J
    #BSUB -eo 'output_dir/jobname'.e%J

    e.g. for qsub (MOAB/TORQ):
    #PBS -N 'jobname'

    thus, the job id number is appended to the output files:
      e.g.: jobs/mySim.o123456, jobs/mySim.e123456



sub_queue  [optional]

    Adds a queue directive into the submission script. This depends on the configuration of your cluster.

    #TAG -q 'queue'



sub_parameters  [optional]

    This may hold any number of options passed to the scheduler, within parentheses and comma-separated. Each option is then written separately in the submission script:

    #TAG option1
    #TAG option2
    ...

    Typical parameters (options) are of the form: "--option-name=value" or "-x value"

    e.g.
    sub_parameters (--time=8:00:00, --mem-per-cpu=6000, --mail-type=FAIL, --mail-user=me, -p arg, --project=xyz)
    or
    sub_parameters --execution-queue=small
becomes:

    #TAG --time=8:00:00
    #TAG --mem-per-cpu=6000
    #TAG --mail-type=FAIL
    #TAG --mail-user=me

or,
    sub_parameters (--project myteam)

becomes

    #OAR --project myteam



sub_resources  [optional]

    Similar to 'sub_parameters' but for resource specifications given as a single string (which contains commas). Can hold only ONE string. It will be written in the submission script as:

    #TAG -l 'resource-string'

a typical resource query is of the form (here for PBS/qsub):

    sub_resources nodes=1:ppn=8,walltime=1:00:00,mem=8gb,file=40gb

which becomes, in the submission script (here for PBS/qsub):

    #PBS -l nodes=1:ppn=8,walltime=1:00:00,mem=8gb,file=40gb

For OAR:

    sub_resources "{memnode=32000}/nodes=8/cpu=1/core=1,walltime=8:0:0"

    #OAR -l "{memnode=32000}/nodes=8/cpu=1/core=1,walltime=8:0:0"

NOTE: any other option specifications that do not match with the two above procedures can be placed in the script template called `sub_template`.


----------------------------
NEMOSUB PARAMETER EXPANSION WITH DUMMY PARAMETERS:

    nemosub allows for some basic level of "scripting" in the nemo input parameter file (init file) when using "dummy" parameters and their value-expansion using nemo's parameter value expansion mechanism developed for so-called 'sequential' parameters. A parameter is said sequential when it is given several values in the init file. Nemo then has a mechanism to initiate as many simulations as the number of parameter value combinations and to substitute specific character strings within other parameter arguments with the values of the sequential parameters. Such "expansion strings" are of the form `%2`, `%'2[aaBBccDd]'1 (see Nemo manual) and are typically used to craft simulation-specific file names and paths.

    nemosub accepts any parameter in the input init file, meaning that if one adds a fake parameter that does not belong to any simulation component, nemosub will still treat it as a regular parameter and use parameter expansion in case that parameter has multiple values. This is handy when having to adjust paths to parameter files or source populations for different cases. Here is an example where we have simulated the adaptation of plant species to different rates of climate change on different spatial grids. We added a 'grid' and 'scenario' parameter to adjust the paths to specific external files storing landscape grids.

eg. of an init file with nemosub and dummy parameters:

    ## NEMOSUB CONFIG ##

    ## job scheduling parameters
    sub_program                     oarsub             # scheduler command
    sub_script_directive            OAR
    sub_parameters                  (--project teamx)
    sub_resources                   nodes=1/core=8,walltime=96:00:00
    sub_script_args                 nemoage0.31.0      # nemo executable name passed as argument to submission script
    sub_input_dir                   $HOME/jobs/init
    sub_output_dir                  $HOME/jobs

    sub_jobname                     camp%'.4'2W33g%1s%3

    ### DUMMY PARAMETERS ###
    scenario                        1 2 3              # referenced as %3
    grid                            1 2 3 4 5 6 11     # referenced as %1

    ## NEMO CONFIG ##

    filename                        s%3_campanula_mut%'.4'2_VS33_g%1

    root_dir                        /tmp/grid%1        # grid-specific output directory

    run_mode                        overwrite
    random_seed                     254

    ## SOURCE POP
    source_pop                      /data/range-shift/simuls/popini/campanula_%'.4'2_g%1_033W2

    ## SELECTION
    selection_local_optima          &init/currentE_grid%1.txt
    rate_environmental_change       (@g0 &init/rate/constantE.txt, @g10 &init/rate/rate_2020_s%3_%1.txt, ...)

    ##QUANTITATIVE TRAITS
    quanti_mutation_rate            0.01 0.001 0.0001  # sequential parameter %2

    [...]
