#!/bin/bash

## POSITIONAL PARAMETERS 
# they refer to the arguments passed to the script:
# these arguments are passed by nemosub
#$1 = init file                 # first argument is always added by nemosub
#$2 = nemo exec (without path)  # second argument passed by nemosub only if "sub_script_args" is present in the init file

## Nemosub will add the SBATCH directives here ##

export SLURM_WHOLE=1

hostname

# name of the parameter file passed as argument to the script
initname=`basename $1`

# extract the filename of the simulations from the input parameter file
simname=`awk '/^filename/{if($2 != "")print $2}' $1`

# extract the root directory of the simulations from the input parameter file
root_dir=`awk '/^root_dir/{print $2}' $1` 

# print info
echo "starting sim in: " $1

# build the command to submit the job:  srun nemox.y.z_mpi parameter-file.ini
# note that for MPI jobs, we have to use srun on a SLURM cluster (use mpirun otherwise?)
command="srun $2 $1"

# print info
echo "executing $command... "

# submit the job
$command

# checks after execution
if [ $? -ne 1 ];
   then
    echo "normal exit: " $1
    rm $1

# postprocessing:

# Tar-gzip result files:
    #tar cfz  ${simname}.tgz $root_dir/$simname* --remove-files
    #tar cfz  ${simname}.tgz $root_dir{/,/ntrl/,/quanti/}$simname* --remove-files

# or just compress files:
    #gzip $root_dir/${simname}*.*

   else

    echo "abnormal exit: " $1
#    mv $1 failed-jobs/$initname
fi


